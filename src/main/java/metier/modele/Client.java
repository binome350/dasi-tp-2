package metier.modele;

import com.google.maps.model.LatLng;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import util.GeoTest;

@Entity
public class Client implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nom;
    private String prenom;
    
    @Column(unique = true)
    private String mail;
    
    private String adresse;
    private Double longitude;
    private Double latitude;
    
    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private Collection<Commande> commandes;

    protected Client() {
        this.commandes = new ArrayList<>();
    }
    
    public Client(String nom, String prenom, String mail, String adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        
        this.adresse = adresse;
        this.setLatitudeLongitude();
        this.commandes = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMail() {
        return mail;
    }

    public String getAdresse() {
        return adresse;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
        this.setLatitudeLongitude();
    }

    public void setLatitudeLongitude() {
        LatLng pos = GeoTest.getLatLng(adresse);
        if(pos != null){
            longitude = pos.lng;
            latitude = pos.lat;
        } else {
            longitude = null;
            latitude = null;
            System.err.println("ADRESSE INCONNUE : PAS DE POSITION RETOURNEE");
        }
    }
    
    public void setLatitudeLongitude(double lat, double lng) {
        longitude = lng;
        latitude = lat;
    }

    public Collection<Commande> getCommandes() {
        return commandes;
    }

    public void addCommande(Commande commandes) {
        this.commandes.add(commandes);
    }

    @Override
    public String toString() {
        return "Client{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", adresse=" + adresse + ", longitude=" + longitude + ", latitude=" + latitude + '}';
    }

}
