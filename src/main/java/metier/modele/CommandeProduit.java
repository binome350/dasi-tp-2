/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.modele;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author sbarkat
 */
@Entity
public class CommandeProduit implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private long quantite;
    
    @ManyToOne
    protected Produit produit;
    
    public CommandeProduit() {
        
    }

    public CommandeProduit(long quantite, Produit produit) {
        this.quantite = quantite;
        this.produit = produit;
    }

    public long getQuantite() {
        return quantite;
    }

    public Produit getProduit() {
        return produit;
    }
    
    public String toString() {
        return produit.getDenomination()+" - Qté: "+quantite;
    }
    
}
