/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.modele;

import com.google.maps.model.LatLng;
import javax.persistence.Entity;
import util.GeoTest;

/**
 *
 * @author sbarkat
 */
@Entity
public class Drone extends Livreur {
    
    protected double vitesse_moyenne;
    
    protected boolean etat;
    
    protected String modele;
    
    public Drone() {
        super();
    }

    public Drone(double vitesse_moyenne, boolean etat, String modele, String email, boolean disponible, double poids_max, String adresse) {
        super(email, disponible, poids_max, adresse);
        
        this.vitesse_moyenne = vitesse_moyenne;
        this.etat = etat;
        this.modele = modele;
    }

    public double getVitesse_moyenne() {
        return vitesse_moyenne;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getModele() {
        return modele;
    }

    @Override
    public double calculerTemps(LatLng posRestaurant, LatLng destination) {
        if (dernierTempsCalc == null) {
            LatLng origin = new LatLng(latitude, longitude);

            double distance = GeoTest.getFlightDistanceInKm(origin, posRestaurant);
            distance += GeoTest.getFlightDistanceInKm(posRestaurant, destination);

            dernierTempsCalc = (distance/vitesse_moyenne)*60;
        }
        
        return dernierTempsCalc;
    }

    @Override
    public String getDenomination() {
        return modele+"-"+ID;
    }
    
}
