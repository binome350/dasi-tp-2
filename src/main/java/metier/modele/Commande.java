/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author sbarkat
 */
@Entity
public class Commande implements Serializable {
    
    public static final String ETAT_TERMINE = "completed";
    public static final String ETAT_EN_COURS = "pending";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String etat;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_initiale;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_fin;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_estimee;
    
    private double prix;
    
    @ManyToOne
    private Client client;
    
    @ManyToOne
    private Restaurant restaurant;
    
    @ManyToOne
    private Livreur livreur;
    
    @OneToMany
    private Collection<CommandeProduit> commandeProduits;
    
    public Commande() {
        commandeProduits = new ArrayList<>();
    }
    
    public Commande(String etat, Date date_initiale, Date date_estimee, double prix, Client client, Restaurant restaurant) {
        this.etat = etat;
        this.date_initiale = date_initiale;
        this.date_estimee = date_estimee;
        this.prix = prix;
        this.client = client;
        this.restaurant = restaurant;
        this.commandeProduits = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Date getDate_initiale() {
        return date_initiale;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public Date getDate_estimee() {
        return date_estimee;
    }

    public void setDate_estimee(Date date_estimee) {
        this.date_estimee = date_estimee;
    }

    public double getPrix() {
        return prix;
    }

    public Client getClient() {
        return client;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Collection<CommandeProduit> getProduits() {
        return commandeProduits;
    }
    
    public void addProduits(CommandeProduit cp) {
        commandeProduits.add(cp);
    }
    
    public void setLivreur(Livreur l) {
        this.livreur = l;
    }
    
    public Livreur getLivreur() {
        return this.livreur;
    }
    
    public String toString() {
        String a = ((id == null) ? "" : "Commande n°"+id+"\r\n")
                + "   Date/heure : " + getDate_initiale() + "\r\n"
                + "   Date/heure estimée de livraison : " + getDate_estimee() + "\r\n"
                + "   Livreur: " + livreur + "\r\n"
                + "   Restaurant: " + restaurant.getDenomination() + "\r\n"
                + "   Client:\r\n"
                + "       " + client.getPrenom() + " " + client.getNom() + "\r\n"
                + "       " + client.getAdresse() + "\r\n"
                + "\r\n\r\n"
                + "Commande: \r\n";

        for (CommandeProduit cp : commandeProduits) {
            a += "  " + cp.getQuantite() + " " + cp.getProduit().getDenomination() + " (" + cp.getQuantite() * cp.getProduit().getPrix() + " €)" + "\r\n";
        }

        a += "\r\n" + "TOTAL : " + getPrix() + "€";

        return a;
    }
    
}
