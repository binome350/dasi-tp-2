/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.modele;

import com.google.maps.model.LatLng;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;
import util.GeoTest;

/**
 *
 * @author sbarkat
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Livreur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long ID;
    
    protected String email;
    
    protected boolean disponible;
    
    protected double poids_max;
    
    protected String adresse;
    
    protected double latitude;
    protected double longitude;
    
    @Version
    protected int version;
    
    @Transient
    protected Double dernierTempsCalc;
    
    @OneToMany(mappedBy="livreur", cascade = CascadeType.ALL)
    protected Collection<Commande> commandes;
    
    public Livreur() {
        this.dernierTempsCalc = null;
        this.commandes = new ArrayList<>();
    }

    public Livreur(String email, boolean disponible, double poids_max, String adresse) {
        this.dernierTempsCalc = null;
        this.email = email;
        this.disponible = disponible;
        this.poids_max = poids_max;
        this.adresse = adresse;
        this.setLatitudeLongitude();
        this.commandes = new ArrayList<>();
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public double getPoids_max() {
        return poids_max;
    }

    public void setPoids_max(double poids_max) {
        this.poids_max = poids_max;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatitudeLongitude() {
        LatLng pos = GeoTest.getLatLng(adresse);
        longitude = pos.lng;
        latitude = pos.lat;
    }
    
    public Collection<Commande> getCommandes() {
        return this.commandes;
    }
    
    public void addCommande(Commande commande) {
        this.commandes.add(commande);
    }
    
    public abstract String getDenomination();
    
    public String toString() {
        return getDenomination() + " (n°"+ID+")";
    }
    
    public abstract double calculerTemps(LatLng posRestaurant, LatLng destination);
}
