/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.modele;

import com.google.maps.model.LatLng;
import javax.persistence.*;
import util.GeoTest;

/**
 *
 * @author sbarkat
 */
@Entity
public class Employe extends Livreur {
    
    protected String prenom;
    protected String nom;

    public Employe() {
        super();
    }
    
    public Employe(String prenom, String nom, String adresse, String email, boolean disponible, double poids_max) {
        super(email, disponible, poids_max, adresse);
        
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }
    
    // TODO: justifier pourquoi on met pas certains setters

    @Override
    public double calculerTemps(LatLng posRestaurant, LatLng destination) {
        if (dernierTempsCalc == null) {
            LatLng origin = new LatLng(latitude, longitude);
        
            dernierTempsCalc = GeoTest.getTripDurationByBicycleInMinute(origin, destination, posRestaurant);
        }
        //DEBUG // 
        System.err.println(" Renvoie de : " + dernierTempsCalc);
        return dernierTempsCalc;
    }

    @Override
    public String getDenomination() {
        return prenom+" "+nom;
    }
    
}
