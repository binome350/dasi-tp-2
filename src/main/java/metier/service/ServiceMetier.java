/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.service;

import com.google.maps.model.LatLng;
import dao.*;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.CommandeProduit;
import metier.modele.Livreur;
import metier.modele.Restaurant;
import util.AppConfig;
import util.LivreurComparator;
import util.exception.CreationCommandeException;
import util.exception.LivreurUnavailableException;

/**
 *
 * @author sbarkat
 */
public class ServiceMetier {

    protected ServiceTechnique outils;

    public ServiceMetier() {
        outils = new ServiceTechnique();
    }

    /*
        PARTIE CLIENTS
     */
    /**
     * Fait persister l'OM client passé en paramètre. En cas de problème,
     * soulève une exception.
     *
     * @return
     */
    public void creerClient(Client client) throws Exception {
        ClientDAO dao = new ClientDAO();

        JpaUtil.creerEntityManager();

        try {
            JpaUtil.ouvrirTransaction();

            dao.create(client);

            JpaUtil.validerTransaction();

            outils.envoyerMail(
                    AppConfig.COMPANY_EMAIL,
                    client.getMail(),
                    "Inscription Terminée !",
                    "Bonjour, " + client.getPrenom() + " " + client.getNom() + ", \n\n"
                    + "Votre inscription sur Gustat'IF est terminée !\n"
                    + "Il vous suffit d'utiliser cette adresse mail pour"
                    + " vous connecter. \n\n"
                    + "En vous souhaitant bonne dégustation,\n"
                    + "Gustat'IF");

        } catch (Exception e) {
            JpaUtil.fermerEntityManager();
            throw e;
        }

        JpaUtil.fermerEntityManager();
    }

    public Client trouverClient(String email) {
        ClientDAO dao = new ClientDAO();
        Client client = null;

        JpaUtil.creerEntityManager();

        try {
            client = dao.findByEmail(email);
        } catch (Exception e) {
            System.err.println(e);
        }

        JpaUtil.fermerEntityManager();

        return client;
    }

    public Client trouverClientParId(long id) {
        ClientDAO dao = new ClientDAO();
        Client client = null;

        JpaUtil.creerEntityManager();

        try {
            client = dao.findById(id, Client.class);
        } catch (Exception e) {
            System.err.println(e);
        }

        JpaUtil.fermerEntityManager();

        return client;
    }

    /**
     * Retourne la liste des clients, ou null en cas de problèmes.
     *
     * @return
     */
    public List<Client> listeClients() {

        ClientDAO dao = new ClientDAO();

        JpaUtil.creerEntityManager();

        List<Client> liste = null;

        try {
            liste = dao.findAll();

        } catch (Exception e) {
            //println("Impossible de récupérer la liste de clients.");

        }

        JpaUtil.fermerEntityManager();

        return liste;
    }

    /*
        PARTIE RESTAURANTS
     */
    /**
     * Retourne la liste des restaurants, ou null en cas de problèmes.
     *
     * @return
     */
    public List<Restaurant> listeRestaurants() {

        RestaurantDAO dao = new RestaurantDAO();

        JpaUtil.creerEntityManager();

        List<Restaurant> liste = null;

        try {
            liste = dao.findAll();

        } catch (Exception e) {
            //println("Impossible de récupérer la liste de produits.");
        }

        JpaUtil.fermerEntityManager();

        return liste;
    }

    public Restaurant trouverRestaurantParId(long id) {
        RestaurantDAO dao = new RestaurantDAO();
        Restaurant restaurant = null;
        System.err.println("DANS L'AUTRE PROJET ! ");
        JpaUtil.creerEntityManager();

        try {
            restaurant = dao.findById(id, Restaurant.class);
        } catch (Exception e) {
            System.err.println(e);
        }
        System.err.println("RETOUR DANS NOTRE PROJET ! ");

        JpaUtil.fermerEntityManager();

        return restaurant;
    }

    /*
        PARTIE COMMANDE
     */
    /**
     * Persiste une commande et lui attribue le meilleur livreur possible. Si
     * aucun livreur n'est disponible, la commande est annulée en soulevant une
     * exception.
     *
     * @param commande
     * @param client
     * @throws LivreurUnavailableException Aucun livreur de disponible pour la
     * commande.
     */
    public void creerCommande(Commande commande, Client client, double poids) throws LivreurUnavailableException, CreationCommandeException {
        JpaUtil.creerEntityManager();
        ClientDAO dao = new ClientDAO();

        client.addCommande(commande);

        Restaurant restaurant = commande.getRestaurant();

        // PARTIE DETERMINATION LIVREUR
        List<Livreur> livreurs = trouverLivreursPourCommande(commande, poids);

        /**
         * Principe de l'algorithme de détermination du meilleur livreur: On
         * récupère la liste des livreurs aptes à la livraison de cette
         * commandes, du plus rapide au plus lent.
         *
         * On va donc commencer par sélectionner le premier de la liste. On
         * écrit la date estimée de livraison dans la commande, et on marque le
         * livreur comme indiponible.
         *
         * On valide ensuite la transaction. Si cette dernière échoue (par ex.
         * le livreur a déjà été attribué), on sélectionne alors le prochain
         * livreur de la liste, et on recommence jusqu'à trouver un livreur
         * apte.
         *
         * S'il n'y en a pas, la commande est annulée.
         */
        boolean livreurOk = false;
        boolean arretCritique = false;
        Livreur livreurChoisi;
        int i = 0;
        while (!arretCritique && !livreurOk && i < livreurs.size()) {
            JpaUtil.ouvrirTransaction();
            try {
                livreurChoisi = livreurs.get(i);

                commande.setLivreur(livreurChoisi);

                long currentMillis = Calendar.getInstance().getTimeInMillis();

                LatLng posRestaurant = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
                LatLng posClient = new LatLng(client.getLatitude(), client.getLongitude());

                commande.setDate_estimee(new Date(currentMillis + (long) (60000 * livreurChoisi.calculerTemps(posRestaurant, posClient))));
                livreurChoisi.setDisponible(false);

                dao.update(client);

                livreurOk = true;

                if (AppConfig.TEST_CONCURRENCY) {
                    System.out.println("Appuyez sur entrée pour confirmer la commande.");
                    (new Scanner(System.in)).nextLine();
                }

                JpaUtil.validerTransaction();

                String mailCorps
                        = "Bonjour " + livreurChoisi.getDenomination() + ",\r\n"
                        + "Merci d'effectuer cette livraison dès maintenant, tout en respectant le code de la route ;-)\r\n\r\n"
                        + "Le Chef (Cuisto... ok on m'a déjà viré pour cette blague.)\r\n\r\n"
                        + "Détails de la livraison\r\n"
                        + commande;

                outils.envoyerMail(
                        AppConfig.COMPANY_EMAIL,
                        livreurChoisi.getEmail(),
                        "Livraison à effectuer",
                        mailCorps
                );
                // ENVOI MAIL

            } catch (OptimisticLockException | RollbackException e) {
                if (AppConfig.DEBUG) {
                    System.err.println("Erreur lors de la création d'une commande !");
                    System.err.println(e);
                    e.printStackTrace();
                }
                i++;
                livreurOk = false;
                commande.setEtat(Commande.ETAT_EN_COURS);
                JpaUtil.annulerTransaction();
            } catch (Exception ex) {
                if (AppConfig.DEBUG) {
                    Logger.getLogger(ServiceMetier.class.getName()).log(Level.SEVERE, null, ex);
                }

                JpaUtil.annulerTransaction();
                arretCritique = true;

            }
        }

        JpaUtil.fermerEntityManager();

        if (!livreurOk) {
            throw new LivreurUnavailableException("Aucun livreur n'est disponible pour votre commande.");
        }

        if (arretCritique) {
            throw new CreationCommandeException("La commande n'a pu être créée. Veuillez réessayer ultérieurement.");
        }
    }

    /**
     * Détermine, en fonction de leur disponibilité et de leur poids maximal de
     * transport, les livreurs aptes à effectuer la commande. La liste des
     * livreurs est ensuite triée selon le temps nécessaire pour effectuer la
     * commande.
     *
     * @param commande
     * @param poids
     * @return Liste des livreurs triée selon le nombre de minutes pour
     * effectuer le trajet.
     * @throws LivreurUnavailableException
     */
    protected List<Livreur> trouverLivreursPourCommande(Commande commande, double poids) throws LivreurUnavailableException {

        // On ne crée pas d'entity manager car cette méthode n'est JAMAIS appellée directement.
        LivreurDAO dao = new LivreurDAO();

        try {
            // On récupère la liste des livreurs disponible pour ce poids
            List<Livreur> livreurs = (List<Livreur>) dao.findAvailables(poids, true);

            // On trie la liste selon les critères défninis auparavant...
            Collections.sort(livreurs, new LivreurComparator(commande.getClient(), commande.getRestaurant()));

            if (AppConfig.DEBUG) {
                for (Livreur l : livreurs) {
                    System.out.println(l.toString());
                }
            }

            return livreurs;

        } catch (Exception ex) {
            if (AppConfig.DEBUG) {
                System.err.println(ex);
                ex.printStackTrace();
            }

            throw new LivreurUnavailableException("Aucun livreur n'a pu être trouvé.");
        }
    }

    /**
     * Rend à nouveau le livreur concerné par la commande disponible et met à
     * jour le statut de la commande à "Terminé".
     *
     * @param commandeEnCours
     * @return Mise à jour persistée ou non
     */
    public boolean conclureCommande(Commande commandeEnCours) {
        JpaUtil.creerEntityManager();

        boolean ok = false;

        try {
            commandeEnCours.setEtat(Commande.ETAT_TERMINE);
            commandeEnCours.getLivreur().setDisponible(true);

            JpaUtil.ouvrirTransaction();

            CommandeDAO dao = new CommandeDAO();
            dao.update(commandeEnCours);

            JpaUtil.validerTransaction();

            ok = true;

        } catch (Exception ex) {
            System.err.println("Impossible de mettre à jour la commande !");
            Logger.getLogger(ServiceMetier.class.getName()).log(Level.SEVERE, null, ex);
            JpaUtil.annulerTransaction();

            ok = false;
        }

        JpaUtil.fermerEntityManager();

        return ok;
    }

    /**
     * Récupère la liste des commandes en cours. Si une erreur survient, alors
     * renvoit null.
     *
     * @return Liste des commandes en cours, ou null.
     */
    public List<Commande> listeCommandesEnCours() {
        JpaUtil.creerEntityManager();

        List<Commande> commandes = null;

        try {
            CommandeDAO dao = new CommandeDAO();

            commandes = dao.findByEtat(Commande.ETAT_EN_COURS);
        } catch (Exception ex) {
            if (AppConfig.DEBUG) {
                Logger.getLogger(ServiceMetier.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        JpaUtil.fermerEntityManager();

        return commandes;
    }

    /**
     * Récupère une commande grâce à son ID. Si introuvable, renvoie null.
     *
     * @param id
     * @return
     */
    public Commande trouverCommandeParId(long id) {
        CommandeDAO dao = new CommandeDAO();
        Commande commande = null;

        JpaUtil.creerEntityManager();

        try {
            commande = dao.findById(id);
        } catch (Exception e) {
            System.err.println(e);
        }

        JpaUtil.fermerEntityManager();

        return commande;
    }

    /*
        PARTIE LIVREUR
     */
    /**
     * Récupère un OM livreur à partir de son adresse email. S'il est
     * introuvable, on récupère alors une valeur nulle.
     *
     * @param email
     * @return Livreur ou null
     */
    public Livreur trouverLivreur(String email) {

        JpaUtil.creerEntityManager();
        Livreur l = null;

        LivreurDAO dao = new LivreurDAO();

        try {
            l = dao.findByEmail(email);
        } catch (Exception ex) {
            if (AppConfig.DEBUG) {
                Logger.getLogger(ServiceMetier.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        JpaUtil.fermerEntityManager();

        return l;
    }

    /**
     * Récupère un livreur grâce à son ID. Si introuvable, renvoie null.
     *
     * @param id
     * @return
     */
    public Livreur trouverLivreurParId(long id) {
        LivreurDAO dao = new LivreurDAO();
        Livreur livreur = null;

        JpaUtil.creerEntityManager();

        try {
            livreur = dao.findById(id, Livreur.class);
        } catch (Exception e) {
            System.err.println(e);
        }

        JpaUtil.fermerEntityManager();

        return livreur;
    }

    /**
     * Récupère la liste des livreurs. Si une erreur survient, alors renvoit
     * null.
     *
     * @return Liste des livreurs, ou null.
     */
    public List<Livreur> listeLivreurs() {

        LivreurDAO dao = new LivreurDAO();

        JpaUtil.creerEntityManager();

        List<Livreur> liste = null;

        try {
            liste = dao.findAll();
        } catch (Exception ex) {
            if (AppConfig.DEBUG) {
                Logger.getLogger(ServiceMetier.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        JpaUtil.fermerEntityManager();

        return liste;
    }

}
