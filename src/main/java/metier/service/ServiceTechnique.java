 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.service;

import dao.JpaUtil;
import dao.LivreurDAO;
import metier.modele.Drone;
import metier.modele.Employe;
import metier.modele.Livreur;

/**
 *
 * @author sbarkat
 */
public class ServiceTechnique {
    
    public void envoyerMail(String from, String to, String subject, String message) {
        System.out.println();
        System.out.println("---------------- MAIL ----------------");
        System.out.println("De : "+from);
        System.out.println("À : "+to);
        System.out.println("Sujet : "+subject);
        System.out.println("Contenu :");
        System.out.println(message);
        System.out.println("-------------- FIN MAIL --------------");
        System.out.println();
        
    }
    
    public void creerLivreur() {
        LivreurDAO dao = new LivreurDAO();
        
        Livreur[] tabLivreurs = new Livreur[50];
        
        for (int i=0; i<tabLivreurs.length; i++) {
            tabLivreurs[i] = livreurAleatoire(i>34); // on a une 15aine de drones
        }
        
        JpaUtil.creerEntityManager();
        
        try {
            JpaUtil.ouvrirTransaction();
            
            for (Livreur l: tabLivreurs) {
                dao.create(l);
            }
           
            JpaUtil.validerTransaction();
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        
        JpaUtil.fermerEntityManager();
        
        System.out.println("Livreurs générés.");
    }
    
    protected Livreur livreurAleatoire(boolean estUnDrone) {
        String[] rues = new String[]{
            "avenue Albert Einstein 69100 Villeurbanne",
            "Rue des Charmettes, 69006 Lyon",
            "Rue de l'Abbé Rozier, 69001 Lyon",
            "Avenue Jean Capelle O, 69100 Villeurbanne",
            "Rue du Bocage, 69008 Lyon",
            "Rue Paul Chenavard, 69001 Lyon",
            "Quai de Bondy, 69005 Lyon",
            "Rue Belfort, 69004 Lyon",
            "Rue Louis Guérin, 69100 Villeurbanne",
            "Rue de Château Gaillard, 69100 Villeurbanne",
        };
        
        String[] prenoms = new String[] {
            "Louis",
            "Marie",
            "Emma",
            "Carie",
            "Baptiste",
            "Marwa",
            "Jean",
            "Estelle",
            "Pierette",
            "Michel",
            "Inès",
            "Sarah",
        };
        
        String[] noms = new String[] {
            "De Broglie",
            "Feras",
            "Tupol",
            "Petitpont",
            "Forever",
            "Resmur",
            "Nlow",
            "Férir"
        };
        
        String[] nomDrones = new String[] {
            "UltraDominator",
            "Dronix 2000",
            "DroneBoy",
            "DroneGirl",
            "MiniDrony",
            "GigaDrony",
            "Vincecator",
            "PilotMaster",
            "MacroDrone",
            "SuperDrone"
        };
        
        String[] domainesEmail = new String[] {
            "quantum.phy",
            "onsa-lyon.fr",
            "mgail.com",
            "aol.com",
            "cash.fr",
            "forever.fr",
            "wanadoo.com",
        };
        
        if (!estUnDrone) {
            String nom = noms[(int)(Math.random()*noms.length)],
                prenom = prenoms[(int)(Math.random()*prenoms.length)],
                
                adresse = (int)(Math.random()*30)
                +" "+rues[(int)(Math.random()*rues.length)],
                
                email = prenom+"."+nom+"."
                +(int)(Math.random()*6000)+"@"
                +domainesEmail[(int)(Math.random()*domainesEmail.length)];
            
            email = email.toLowerCase();
                
            int poidsMax = (int)(19000*Math.random()+1000);
                
            return new Employe(prenom, nom, adresse, email, true, poidsMax);
        } else {
            String modele = nomDrones[(int)(Math.random()*nomDrones.length)],
                    email = modele+"."+(int)(Math.random()*2000)+"@drone-service.fr",
                    adresse = "251 Rue Marcel Mérieux, 69007 Lyon";
            
            email = email.toLowerCase();
            
            int poidsMax = (int) (500+2500*Math.random()),
                    vitesseMoy = (int)(20+10*Math.random());
            
            return new Drone(vitesseMoy, true, modele, email, true, poidsMax, adresse);
        }
    }
    
}
