package dao;

import java.util.List;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.Client;

public class ClientDAO extends DAO<Client> {
    
     @Override
    public List<Client> findAll() throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        Query q = em.createQuery("select c FROM Client c");
        
        return (List<Client>) q.getResultList();
        
    }
    
    public Client findByEmail(String mail) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        Client client = null;
        try {
           Query q = em.createQuery("SELECT c FROM Client c WHERE c.mail = :mail");
           q.setParameter("mail", mail);

           client = (Client)q.getSingleResult();
           }
        catch(Exception e) {
               throw e;
        }
        return client;
    }
    
}
