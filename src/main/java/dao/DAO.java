/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author sbarkat
 * @param <Modele>
 */
public abstract class DAO<Modele> {

    public void create(Modele m) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(m);
    }

    public void update(Modele m) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.merge(m);
    }

    public Modele findById(long id, Class<?> referenceType) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        Modele m = null;
        
        // DEBUG // System.out.println("On cherche un " + referenceType + " avec l'ID " + id);
        try {
            m = (Modele) em.find(referenceType, id);
        } catch (Exception e) {
            throw e;
        }
        
        //DEBUG // System.out.println("On a trouvé" + m);
        return m;
    }

    public abstract List<Modele> findAll() throws Exception;
}
