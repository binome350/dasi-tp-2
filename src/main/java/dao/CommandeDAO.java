package dao;

import java.util.List;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.Client;
import metier.modele.Commande;

public class CommandeDAO extends DAO<Commande> {
    
     @Override
    public List<Commande> findAll() throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        Query q = em.createQuery("select c FROM Commande c");
        
        return (List<Commande>) q.getResultList();
        
    }

   
    public Commande findById(long id) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        Commande commande = null;
        try {
            commande = em.find(Commande.class, id);
        }
        catch(Exception e) {
            throw e;
        }
        return commande;
    }
    
    public List<Commande> findByEtat(String etat) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Commande> commandes = null;
        try {
           Query q = em.createQuery("SELECT c FROM Commande c WHERE c.etat = :etat");
           q.setParameter("etat", etat);

           commandes = q.getResultList();
        } catch(Exception e) {
           throw e;
        }
        
        return commandes;
    }
    
}
