/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.modele.Livreur;

/**
 *
 * @author sbarkat
 */
public class LivreurDAO extends DAO<Livreur> {
    public List<Livreur> findAvailables(double poids, boolean disponible) throws Exception {
        
        EntityManager em = JpaUtil.obtenirEntityManager();
        
         Query q = em.createQuery("select c FROM Livreur c WHERE c.poids_max >= :poids AND c.disponible = :dispo");
         q.setParameter("poids", poids);
         q.setParameter("dispo", disponible);
         
         return q.getResultList();
    } 

    @Override
    public List<Livreur> findAll() throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        Query q = em.createQuery("select c FROM Livreur c");
        
        return (List<Livreur>) q.getResultList();
    }
    
    public Livreur findByEmail(String email) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        
        Query q = em.createQuery("select l from Livreur l WHERE l.email = :email");
        q.setParameter("email", email);
        
        Livreur l = (Livreur) q.getSingleResult();
        
        return l;
    }
}
