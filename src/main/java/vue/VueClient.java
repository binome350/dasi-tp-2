/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.modele.Client;
import metier.modele.Commande;

/**
 *
 * @author kevin
 */
public class VueClient extends AppVue {

    @Override
    public Class<?> run(HashMap<String, Object> session) {
        String action = (String) session.get("action");
        
        /**
         * L'idée principale de cette vue est de gérer toutes les actions relatives au client.
         * Ainsi, on retrouve la connexion, l'inscription, mais également la visualisation de ses commandes.
         */
        
        if (action == null) action = "";
        
        Class<?> vueDestination = VueAccueil.class;
        
        switch (action) {
            case "connexion":
                vueDestination = connexion(session);
                break;
            case "inscription":
                vueDestination = inscription(session);
                break;
            case "listeCommandes":
                vueDestination = listeCommandes(session);
                break;
            case "actionsClient":
                vueDestination = menuConnecte(session);
                break;
            default:
                vueDestination = menuConnexion(session);
                break;
        }
        
        return vueDestination;
    }
    
    protected Class<?> menuConnexion(HashMap<String, Object> session) {
        println("Pour utiliser l'application, vous devez vous connecter.");
        int choix = menuNum(new String[]{"Connexion", "Inscription", "Quitter"});
            switch(choix) {
                case 0:
                    session.put("action", "connexion");
                    break;
                case 1:
                    session.put("action", "inscription");
                    break;
                default:
                    return VueAccueil.class;
            }
            
        return VueClient.class;
    }
    
    protected Class<?> menuConnecte(HashMap<String, Object> session) {
        if (session.containsKey("client") && session.get("client") instanceof Client) {
            println("Bienvenue, que souhaitez-vous faire ?");
            int choix = menuNum(new String[]{"Faire une nouvelle commande", "Consulter mes précédentes commandes", "Retour à l'accueil"});
            switch(choix) {
                case 0:
                    return VueCommande.class;
                case 1:
                    session.put("action", "listeCommandes");
                    return VueClient.class;
                case 2:
                default:
                    return VueAccueil.class;
            }
        } else {
            println("Erreur de session. Vous n'êtes pas censé pouvoir accéder à ce module.");
            return VueAccueil.class;
        }
    } 
    
    protected Class<?> connexion(HashMap<String, Object> session) {
        println("Entrez votre adresse mail pour vous connecter.");
        print("Email: ");
        
        String email = sc.nextLine();
        
        Client client = service.trouverClient(email);
        
        if (client == null) {
            println("Identifiants introuvables. Redirection vers l'accueil..");
            session.remove("action");
        } else {
            session.put("client", client);
            println("Connexion effectuée ! Bienvenue, "+client.getPrenom()+" "+client.getNom());
            session.put("action", "actionsClient");
        }
        
        return VueClient.class;
    }
    
    protected Class<?> inscription(HashMap<String, Object> session) {
        
        String nom, prenom, mail, adresse;
        boolean registerOk = false;
        
        println("Bien le bonjour ! Bienvenue sur Gustat'IF !");
        println("Afin d'utiliser nos services, veuillez remplir ce forumlaire d'inscription.");
        
        print("Nom: ");
        nom = sc.nextLine();
        
        print("Prenom: ");
        prenom = sc.nextLine();
        
        print("Email: ");
        mail = sc.nextLine();
        
        print("Adresse (sur une ligne): ");
        adresse = sc.nextLine();
        
        Client client = new Client(nom, prenom, mail, adresse);
        try {
            service.creerClient(client);
            session.put("client", client);
            println("Merci d'avoir utilisé le formulaire d'inscription !");
            session.put("action", "actionsClient");
        } catch (Exception ex) {
            println("L'inscription n'a pu être réalisée ! :( Redirection vers la homepage...");
            session.remove("action");
        }
        
        return VueClient.class;
    }

    protected Class<?> listeCommandes(HashMap<String, Object> session) {
        
        session.put("action", "actionsClient");
        
        println();
        Client client = (Client) session.get("client");
        Collection<Commande> commandes = client.getCommandes();
        if (commandes.size() == 0) {
            println("Vous n'avez pas encore commandé... Mais il est encore temps :-)");
        } else {
            println("Vous avez "+commandes.size()+" commandes");
            Iterator<Commande> it;
            Commande c;
            for (it = commandes.iterator(); it.hasNext();) {
                c = it.next();
                println(c.toString());
                printdelim();
            }
        }
        
        return VueClient.class;
    }
    
}
