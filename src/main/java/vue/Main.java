/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import dao.ClientDAO;
import dao.JpaUtil;
import dao.RestaurantDAO;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.modele.Client;
import metier.modele.Produit;
import metier.modele.Restaurant;
import metier.service.ServiceMetier;

/**
 * Interface d'entrée de l'application Gustat'IF
 */
public class Main {
    
    private Scanner sc;
    private ServiceMetier service;
    
    public Main() {
        JpaUtil.init();
        
        sc = new Scanner(System.in);
        service = new ServiceMetier();
        
//        afficherRestaurantEtProduits();
        
        JpaUtil.destroy();
        
    }
    
    public static void main(String[] args) {
        
        // Initialisation de JPA
        JpaUtil.init();
        
        // On construit notre conteneur d'objet pour la session en cours
        HashMap<String, Object> session = new HashMap<>();
        
        // Au démarrage, l'application affiche la vue "Accueil".
        Class<?> currentVue = VueAccueil.class;
        
        // Instance de la vue en cours d'affichage.
        AppVue vueInstance;
        
        /**
         * L'idée ici est de permettre de rediriger l'utilisateur vers une autre vue
         * une fois le traitement de la vue terminée. Pour cela, lors de l'exécution
         * de la méthide run d'une vue, cette dernière peut renvoyer la classe de la vue
         * suivante, ou null. Un null équivaut à quitter le programme.
         */
        while (currentVue != null) {
            try {
                // Récupération du constructeur de la vue à afficher
                Constructor<?> constructor = currentVue.getConstructor();
                
                // On l'instancie...
                vueInstance = (AppVue) constructor.newInstance();
                
                // Et on la lance :p
                currentVue = vueInstance.run(session);
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        JpaUtil.destroy();
        
    }
}
