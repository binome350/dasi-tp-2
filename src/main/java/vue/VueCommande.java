/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.Produit;
import metier.modele.Restaurant;
import metier.modele.CommandeProduit;
import util.exception.CreationCommandeException;
import util.exception.LivreurUnavailableException;

/**
 *
 * @author sbarkat
 */
public class VueCommande extends AppVue {
    
    private Client client;

    @Override
    public Class<?> run(HashMap<String, Object> session) {
        client = (Client) session.get("client");
        
        println();
        println("---------------------------------------");
        println("COMMANDE");
        println();
        
        // On demande au client de sélectionner un restaurant
        Restaurant r = afficherRestaurants();
        if (r != null) {
            println("Restaurant sélectionné : "+r.getDenomination());
            
            // Puis de choisir ses produits
            Collection<CommandeProduit> produits = afficherProduitsRestaurants(r);
            
            if (produits != null && produits.size() > 0) {
                try {
                    
                double somme = 0;
                double poids = 0;

                // On calcule le poids et la somme totale de la commande.
                for (CommandeProduit p : produits) {
                    somme += p.getQuantite()*p.getProduit().getPrix();
                    poids += p.getQuantite()*p.getProduit().getPoids();
                }

                metier.modele.Commande commande = new metier.modele.Commande(Commande.ETAT_EN_COURS, new Date(), null, somme, client, r);
                
                for (CommandeProduit p : produits) {
                    commande.addProduits(p);
                }

                // Le service crée la commande et fait toute la paperasse...
                service.creerCommande(commande, client, poids);

                println();
                println("------------------------------------");
                println();

                println("Votre commandé a été enregistrée !");
                println("La livraison est prévue à : "+commande.getDate_estimee());
                println("Récapitulatif de votre commande :");
                String recapCommande = "";
                for (CommandeProduit cp : commande.getProduits()) {
                    recapCommande += "  "+cp.getQuantite()+" "+cp.getProduit().getDenomination()+ " ("+cp.getQuantite()*cp.getProduit().getPrix()+" €)"+"\r\n";
                }
                recapCommande += "\r\n"+"TOTAL : "+commande.getPrix()+"€";

                println(recapCommande);
                
                // On retourne sur la vue client
                // Todo : rediriger sur la liste des commandes ?
                session.put("action", "actionsClient");
                return VueClient.class;
                
                } catch (LivreurUnavailableException e) {
                    // TODO : Gérer l'exception de service.
                    println("Aucun livreur n'est disponible. Veuillez retenter votre commande ultérieurement.");
                    return VueAccueil.class;
                } catch (CreationCommandeException ex) {
                    println(ex.getMessage());
                }
            }
            
            println("Une erreur est survenue durant la sélection des produits du restaurant. Retour à l'accueil.");
            return VueAccueil.class;
        }
        
        println("Aucun restaurant sélectionné. Retour à l'accueil.");
        return VueAccueil.class;
    }
    
    private Restaurant afficherRestaurants() {
        List<Restaurant> listR = service.listeRestaurants();
        
        if (listR != null) {
            println("Voilà la liste des restaurants disponibles :");
            Restaurant r = (Restaurant) menu(listR.toArray());
            
            return r;
        }
        
        return null;
    }
    
    private Collection<CommandeProduit> afficherProduitsRestaurants(Restaurant restau) {
        
        Object[] listProduits;
        listProduits = ((List<Produit>)restau.getProduits()).toArray();
        
        ArrayList<CommandeProduit> listCommandeProduits = new ArrayList<>();
        
        Produit produitChoisi;
        int nbProduitCommande = 0;
        boolean selectionFinie = false;
        
        println();
        println();
        
        if (listProduits == null || listProduits.length == 0) {
            println("Ce restaurant n'a aucun produit disponible. Retour à l'écran de sélection.");
            return null;
        }
        
        println("Quels produits souhaitez-vous commander ?");
       
        while (!selectionFinie) {
            println();
            produitChoisi = (Produit) menu(listProduits);
            print("En quelle quantité voulez-vous commander ce produit ? ");
            
            nbProduitCommande = sc.nextInt();
            
            sc.nextLine();
            
            listCommandeProduits.add(new CommandeProduit(nbProduitCommande, produitChoisi));
            
            println();
            println("Quantité choisie : "+nbProduitCommande+" - Poids pour ce produit : "+nbProduitCommande*produitChoisi.getPoids());
            
            print("Voulez-vous commander un autre produit ? (1. Oui/ 0. Non)");
            selectionFinie = sc.nextInt() != 1;
            
            sc.nextLine();
        }
        
        return listCommandeProduits;
    }
    
}
