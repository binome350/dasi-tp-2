/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.Collection;
import java.util.HashMap;
import metier.modele.Commande;
import metier.modele.Livreur;
import static vue.AppVue.println;
import static vue.AppVue.println;
import static vue.AppVue.println;
import static vue.AppVue.println;

/**
 *
 * @author kevin
 */
public class VueLivreur extends AppVue {

    protected Livreur livreur;
    
    @Override
    public Class<?> run(HashMap<String, Object> session) {
        println("Cher livreur, veuillez entrez vos identifiants: ");
        livreur = connexion();
        
        if (livreur == null) {
            println("Identifiants introuvables. Redirection vers l'accueil.");
            return VueAccueil.class;
        }
        
        int choix;
        String[] menuChoix = new String[] {"Consulter la commande en cours", "Voir les anciennes commandes", "Retour à l'accueil"};
        
        do {
            choix = menuNum(menuChoix);
            switch (choix) {
                case 0:
                    verifierCommandeEnCours();
                    break;
                case 1:
                    listeCommandesPassees();
                    break;
            }
        } while(choix == 0 || choix == 1);
        
        return VueAccueil.class;
    }
    
    public Livreur connexion() {
        println("Entrez votre adresse mail pour vous connecter.");
        print("Email: ");
        
        String email = sc.nextLine();
        
        Livreur l = service.trouverLivreur(email);
        
        return l;
    }
    
    protected void listeCommandesPassees() {
        Collection<Commande> commandes = livreur.getCommandes();
        printdelim();
        
        println("Vous avez "+commandes.size()+" commandes.");
        
        println();
        
        for (Commande c: commandes) {
            println(c.toString());
            printdelim();
        }
    }
    
    protected void verifierCommandeEnCours() {
        Collection<Commande> commandes = livreur.getCommandes();
        printdelim();
        if (commandes.size() > 0) {
            Commande commandeEnCours = (Commande) commandes.toArray()[commandes.size()-1];
            if (commandeEnCours.getEtat() != Commande.ETAT_TERMINE) {
                println("Vous avez une commande en cours !");
                
                println();
                println(commandeEnCours.toString());
                println();
                
                print("Avez-vous terminé cette commande ? (1. Oui / 0. Non) ");
                boolean finished = sc.nextInt() == 1;
                sc.nextLine();
                
                if (finished) {
                    if (service.conclureCommande(commandeEnCours)) {
                        println("La commande a bien été marquée comme terminée.");
                    } else {
                        println("La commande n'a pas pu être finalisée. Une erreur est survenue.");
                    }
                } else {
                    println("Vous avez choisi de ne pas terminer cette commande. Vous pourrez y revenir plus tard.");
                }
                
                return;
            }
        }
        
        println("Vous n'avez pas de commande en cours de livraison.");
        println();
    }
    
}
