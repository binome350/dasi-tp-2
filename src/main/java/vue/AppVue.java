/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;
import metier.service.ServiceMetier;

/**
 * Classe parente à toutes les vues, initialisant les variables nécessaires
 * ainsi que leur logique commune.
 */
public abstract class AppVue {
    
    protected Scanner sc;
    protected ServiceMetier service;
    
    public AppVue() {
        sc = new Scanner(System.in);
        service = new ServiceMetier();
    }
    
    /**
     * Méthode exécutée à chaque affichage d'une vue.
     * @param session Objets de la session en cours
     * @return Classe de la prochaine vue à afficher
     */
    public abstract Class<?> run(HashMap<String, Object> session);
    
    
    protected static void printhead()
    {
        println("   ______                   __            __   _     ____    ____\n" +
                "  / ____/  __  __   _____  / /_  ____ _  / /_ ( )   /  _/   / __/\n" +
                " / / __   / / / /  / ___/ / __/ / __ `/ / __/ |/    / /    / /_  \n" +
                "/ /_/ /  / /_/ /  (__  ) / /_  / /_/ / / /_       _/ /    / __/  \n" +
                "\\____/   \\__,_/  /____/  \\__/  \\__,_/  \\__/      /___/   /_/     \n" +
                "                                                                 ");
    }
    
    /**
     * Affiche un délimiteur
     */
    protected static void printdelim() {
        println();
        println("---------------------------------------");
        println();
    }
    
    /**
     * Saute une ligne
     */
    protected static void println() {
        System.out.println();
    }
    
    /**
     * Affiche un message avec un retour à la ligne
     * @param message 
     */
    protected static void println(String message) {
        System.out.println(message);
    }
    
    /**
     * Affiche un message (sans retour à la ligne)
     * @param message 
     */
    protected static void print(String message) {
        System.out.print(message);
    }
    
    /**
     * Permet d'afficher un tableau d'objet, avec sa position
     * Il est recommandé d'implémenter la méthode toString() aux objets du tableau.
     * @param choices 
     */
    protected void printArray(Object[] choices) {
        for (int i = 0; i<choices.length; i++) {
            println(i+" - "+choices[i]);
        }
    }
    
    /**
     * Permet, à partir d'un tableau d'objet, de demander à un utilisateur de choisir
     * un de ces objets. Cet objet est alors renvoyé par la fonction.
     * @param choices
     * @return Object sélectionné par l'utilisateur
     */
    protected Object menu(Object[] choices) {
        int choice = menuNum(choices);
        
        
        return choices[choice];
    }
    
    protected int menuNum(Object[] choices) {
        int choice = -1;
        
        printArray(choices);
        
        while (choice < 0 || choice >= choices.length) {
            print("Faites votre choix en entrant le numéro correspondant : ");
            choice = sc.nextInt();
            sc.nextLine();
            
            if (choice < 0 || choice >= choices.length) {
                println("Erreur: choix incorrect. Veuillez recommencer.");
            }
        }
        
        return choice;
    }
    
}
