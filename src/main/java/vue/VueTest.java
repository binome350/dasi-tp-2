/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import dao.JpaUtil;
import java.util.HashMap;
import metier.service.ServiceTechnique;

/**
 *
 * @author kevin
 */
public class VueTest extends AppVue {

    @Override
    public Class<?> run(HashMap<String, Object> session) {
        
        println();
        println("Au niveau des choix de test, vous avez le choix...");
        
        int choix = menuNum(new String[]{"Peupler les tables (livreurs)", "Instruction pour peupler les clients/produits/restaurants", "Retour à l'accueil"});
        
        switch(choix) {
            case 0:
                peuplementTables();
                break;
            case 1:
                planTest();
                break;
            case 2:
                return VueAccueil.class;
        }
        
        return VueTest.class;
    }
    
    protected void peuplementTables() {
        ServiceTechnique outils = new ServiceTechnique();
        
        outils.creerLivreur();
    }
    
    protected void planTest() {
        println();
        println("Pour remplir les tables clients/produits/restaurants, vous pouvez utiliser le fichier sql ayant pour chemin:");
        println("src/main/java/sql/base_clients_restaurants_produits.sql (ou plus simplement le package sql)");
    }
    
}
