/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.HashMap;
import java.util.List;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.Livreur;
import metier.modele.Restaurant;

/**
 *
 * @author kevin
 */
public class VueGestion extends AppVue {

    public static final String ACCESS_PASSWORD = "gestiongust";
    
    @Override
    public Class<?> run(HashMap<String, Object> session) {
        
        println("Pour entrer dans l'interface de gestion, veuillez entrer le pass de la société:");
        String entreePass = sc.nextLine();
        
        if (!entreePass.equals(ACCESS_PASSWORD)) {
            println("Accès refusé. Retour à l'accueil.");
            return VueAccueil.class;
        }
        
        println("Accès autorisé. Bienvenue.");
        println();
        
        int choix;
        String[] menuChoix = new String[] {"Consulter les commandes en cours", "Voir la liste des livreurs", "Voir la liste des clients", "Voir la liste des restaurants", "Retour à l'accueil"};
        
        do {
            choix = menuNum(menuChoix);
            switch (choix) {
                case 0:
                    commandesEnCours();
                    break;
                case 1:
                    listeLivreurs();
                    break;
                case 2:
                    listeClients();
                    break;
                case 3:
                    listeRestaurants();
                    break;
            }
        } while(choix >= 0 && choix < 4);
        
        return VueAccueil.class;
        
    }
    
    protected void commandesEnCours() {
        List<Commande> commandes = service.listeCommandesEnCours();
        
        if (commandes.size() == 0) {
            println("Il n'y a aucune commande en cours.");
        } else {
            for (Commande c: commandes) {
                println(c.toString());
            }
        }
    }
    
    protected void listeLivreurs() {
        List<Livreur> livreurs = service.listeLivreurs();
        
        if (livreurs.size() == 0) {
            println("Il n'y a aucun livreur.");
        } else {
            for (Livreur l: livreurs) {
                println(l.toString());
            }
        }
    }
    
    protected void listeClients() {
        List<Client> clients = service.listeClients();
        
        if (clients.size() == 0) {
            println("Il n'y a aucun client.");
        } else {
            for (Client c: clients) {
                println(c.toString());
            }
        }
    }
    
    protected void listeRestaurants() {
        List<Restaurant> restaurants = service.listeRestaurants();
        
        if (restaurants.size() == 0) {
            println("Il n'y a aucun restaurant.");
        } else {
            for (Restaurant r: restaurants) {
                println(r.toString());
            }
        }
    }
    
}
