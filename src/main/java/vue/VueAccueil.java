/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.HashMap;

/**
 * Page d'accueil principale de l'application
 * @author kevin
 */
public class VueAccueil extends AppVue {

    @Override
    public Class<?> run(HashMap<String, Object> session) {
        printhead();
        println("Bonjour, et bienvenue dans Gustat'IF.");
        println("Pour commander de bons petits plats, vous devez vous connecter ou vous inscrire.");
        int choix = menuNum(new String[]{"Je suis un client", "Je suis un livreur", "Je suis un gestionnaire", "Je suis une tulipe (zone test)", "Quitter"});
    
        switch (choix) {
            case 0:
                return VueClient.class;
            case 1:
                return VueLivreur.class;
            case 2:
                return VueGestion.class;
            case 3:
                return VueTest.class;
            case 4:
            default:
                return null;
        }
    }
    
}
