/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.exception;

/**
 * Exception levée lorsqu'aucun livreur n'est disponible pour une tâche.
 * @author kevin
 */
public class LivreurUnavailableException extends Exception {

    public LivreurUnavailableException(String string) {
        super(string);
    }
    
}
