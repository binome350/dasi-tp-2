/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.exception;

/**
 * Exception levée lorsque la création de commande subit une erreur
 * inattendue.
 * @author kevin
 */
public class CreationCommandeException extends Exception {
    
    public CreationCommandeException(String message) {
        super(message);
    }
    
}
