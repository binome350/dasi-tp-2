/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * Classe contenant toutes les informations de configuration nécessaires à l'application
 * 
 * @author kevin
 */
public class AppConfig {
    public static final boolean DEBUG = true;
    public static final boolean TEST_CONCURRENCY = false;
    public static final String COMPANY_EMAIL = "no-reply@gustatif.fr";
}
