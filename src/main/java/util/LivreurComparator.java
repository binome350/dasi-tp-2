/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.google.maps.model.LatLng;
import java.util.Comparator;
import metier.modele.Client;
import metier.modele.Livreur;
import metier.modele.Restaurant;

/**
 *
 * @author kevin
 */
public class LivreurComparator implements Comparator<Livreur> {

    private LatLng posRestaurant;
    private LatLng posClient;

    public LivreurComparator(Client client, Restaurant r) {
        try {
            posClient = new LatLng(client.getLatitude(), client.getLongitude());
        } catch (Exception ex) {
            posClient = new LatLng(0, 0);
            System.err.println("POSITION CLIENT PROBLEMATIQUE : " + ex);
            System.err.println("Position mise à zéro. Client concerné : " + client);
        }
        try {
            posRestaurant = new LatLng(r.getLatitude(), r.getLongitude());
        } catch (Exception ex) {
            posRestaurant = new LatLng(1, 1);
            System.err.println("POSITION RESTAURANT PROBLEMATIQUE : " + ex);
            System.err.println("Position mise à zéro. Restaurant concerné : " + r);
        }
    }

    @Override
    public int compare(Livreur t, Livreur t1) {
        //DEBUG //
        System.err.println("Comparaison de : " + posRestaurant + " avec " + posClient);
        double tempsCalc = t.calculerTemps(posRestaurant, posClient);
        if (AppConfig.DEBUG) {
            System.out.println("[DEBUG] " + tempsCalc + " - " + t);
        }
        return (int) (tempsCalc - t1.calculerTemps(posRestaurant, posClient));
    }

}
